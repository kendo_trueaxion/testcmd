﻿using System.Collections.Generic;
using UnityEditor;

namespace TrueAxion.Build
{
    /// <summary>
    /// Base class for Set PlayerSettings and BuildPlayerOptions to any platform.
    /// </summary>
    public abstract class BaseBuildOptions
    {
        /// <summary>
        /// Abstract class for set PlayerSetting.
        /// </summary>
        /// <param name="platformConfig">Object that contains data for set PlayerSettings.</param>
        public abstract void SetPlayerSetting(BuildConfig platformConfig, int buildNumber);
        /// <summary>
        /// Abstract class for Get BuildPlayerOptions.
        /// </summary>
        /// <param name="platformConfig">Object that contains data for set specific options.</param>
        public abstract BuildPlayerOptions GetPlatformSpecificOptions(BuildConfig platformConfig);

        /// <summary>
        /// First setup for common configuration in BuildPlayerOptions.
        /// </summary>
        protected BuildPlayerOptions GetCommonOptions()
        {
            BuildPlayerOptions options = new BuildPlayerOptions();
            options.scenes = GetCommonScenes();

            return options;
        }

        /// <summary>
        /// Get all scenes in build setting.
        /// </summary>
        /// <returns>Return scenes.</returns>
        private string[] GetCommonScenes()
        {
            List<string> tempScenes = new List<string>();
            foreach (var scene in EditorBuildSettings.scenes)
            {
                if (scene.enabled)
                {
                    tempScenes.Add(scene.path);
                }
            }
            return tempScenes.ToArray();
        }
    }
}
