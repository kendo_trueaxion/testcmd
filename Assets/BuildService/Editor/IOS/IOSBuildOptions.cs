﻿using UnityEditor;

namespace TrueAxion.Build
{
    /// <summary>
    /// Set PlayerSettings and BuildPlayerOptions for iOS.
    /// </summary>
    public class IOSBuildOptions : BaseBuildOptions
    {
        /// <summary>
        /// Set BuildPlayerOptions for iOS.
        /// </summary>
        /// <param name="platformConfig">Object that contains data for set BuildPlayerOptions.</param>
        public override BuildPlayerOptions GetPlatformSpecificOptions(BuildConfig platformConfig)
        {
            BuildPlayerOptions options = GetCommonOptions();

            if (platformConfig.IOS.Scenes != null)
                options.scenes = platformConfig.IOS.Scenes;

            options.target = BuildTarget.iOS;
            options.targetGroup = BuildTargetGroup.iOS;
            options.locationPathName = platformConfig.IOS.ExportPath;

            return options;
        }

        /// <summary>
        /// Set PlayerSttings for iOS.
        /// </summary>
        /// <param name="platformConfig">Object that contains data for set PlayerSettings.</param>
        public override void SetPlayerSetting(BuildConfig platformConfig, int buildNumber)
        {
            PlayerSettings.iOS.appleDeveloperTeamID = platformConfig.IOS.TeamID;
            PlayerSettings.iOS.buildNumber = buildNumber.ToString();
            PlayerSettings.bundleVersion = platformConfig.IOS.Version;

            BuildTargetGroup platform = BuildTargetGroup.iOS;
            PlayerSettings.SetScriptingDefineSymbolsForGroup(platform, string.Join(";", platformConfig.IOS.Defines));
        }
    }
}
