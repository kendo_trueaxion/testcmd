﻿using Newtonsoft.Json;

namespace TrueAxion.Build
{
    /// <summary>
    /// Object that store specific configuration for iOS.
    /// </summary>
    public class IOSBuildConfig : BaseBuildConfig
    {
        /// <summary>
        /// Get data key "teamID" in config file after get iOS platform key.
        /// </summary>
        [JsonProperty("teamID")]
        public string TeamID { get; set; }
    }
}
