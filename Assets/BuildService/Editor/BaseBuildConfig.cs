﻿using Newtonsoft.Json;

namespace TrueAxion.Build
{
    /// <summary>
    /// Base class for configuration platform. Including a common field that every platform should have.
    /// </summary>
    public class BaseBuildConfig
    {
        /// <summary>
        /// Get data key "scenes" in config file after get any platform key.
        /// </summary>
        [JsonProperty("scenes")]
        public string[] Scenes { get; set; }

        /// <summary>
        /// Get data key "exportPath" in config file after get any platform key.
        /// </summary>
        [JsonProperty("exportPath")]
        public string ExportPath { get; set; }

        /// <summary>
        /// Get data key "define" in config file after get any platform key.
        /// </summary>
        [JsonProperty("define")]
        public string[] Defines { get; set; }

        /// <summary>
        /// Get data key "version" in config file after get any platform key.
        /// </summary>
        [JsonProperty("version")]
        public string Version { get; set; }

    }

}
