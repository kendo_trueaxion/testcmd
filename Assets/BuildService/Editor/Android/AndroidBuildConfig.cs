﻿using Newtonsoft.Json;

namespace TrueAxion.Build
{
    /// <summary>
    /// Object that store specific configuration for android.
    /// </summary>
    public class AndroidBuildConfig : BaseBuildConfig
    {
        /// <summary>
        ///  Get data key "keyStore" in config file after get android platform key.
        /// </summary>
        [JsonProperty("keyStore")]
        public KeyConfig KeyStore { get; set; }

        /// <summary>
        ///  Get data key "keyAlias" in config file after get android platform key.
        /// </summary>
        [JsonProperty("keyAlias")]
        public KeyConfig KeyAlias { get; set; }
    }

    /// <summary>
    /// Object that store specific configuration for android.
    /// </summary>
    public class KeyConfig
    {
        /// <summary>
        ///  Get data key "name" in config file after Get keyStore or keyAlias key.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        ///  Get data key "password" in config file after Get keyStore or keyAlias key.
        /// </summary>
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
