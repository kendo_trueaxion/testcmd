﻿using UnityEditor;

namespace TrueAxion.Build
{
    /// <summary>
    /// Set PlayerSettings and BuildPlayerOptions for android.
    /// </summary>
    public class AndroidBuildOptions : BaseBuildOptions
    {
        /// <summary>
        /// Set BuildPlayerOptions for android.
        /// </summary>
        /// <param name="platformConfig">Object that contains data for set BuildPlayerOptions.</param>
        public override BuildPlayerOptions GetPlatformSpecificOptions(BuildConfig platformConfig)
        {
            BuildPlayerOptions options = GetCommonOptions();

            if (platformConfig.Android.Scenes != null)
                options.scenes = platformConfig.Android.Scenes;

            options.target = BuildTarget.Android;
            options.targetGroup = BuildTargetGroup.Android;
            options.locationPathName = platformConfig.Android.ExportPath;

            return options;
        }

        /// <summary>
        /// Set PlayerSttings for android.
        /// </summary>
        /// <param name="platformConfig">Object that contains data for set PlayerSettings.</param>
        public override void SetPlayerSetting(BuildConfig platformConfig, int buildNumber)
        {
            PlayerSettings.Android.keystoreName = platformConfig.Android.KeyStore.Name;
            PlayerSettings.Android.keystorePass = platformConfig.Android.KeyStore.Password;
            PlayerSettings.Android.keyaliasName = platformConfig.Android.KeyAlias.Name;
            PlayerSettings.Android.keyaliasPass = platformConfig.Android.KeyAlias.Password;
            PlayerSettings.Android.bundleVersionCode = buildNumber;
            PlayerSettings.bundleVersion = platformConfig.Android.Version;

            BuildTargetGroup platform = BuildTargetGroup.Android;
            PlayerSettings.SetScriptingDefineSymbolsForGroup(platform, string.Join(";", platformConfig.Android.Defines));
        }
    }
}
