﻿using Newtonsoft.Json;

namespace TrueAxion.Build
{
    /// <summary>
    /// Object that store data for configuration.
    /// </summary>
    public class BuildConfig
    {
        /// <summary>
        /// Get data key "android" in config file.
        /// </summary>
        [JsonProperty("android")]
        public AndroidBuildConfig Android { get; set; }

        /// <summary>
        /// Get data key "iOS" in config file.
        /// </summary>
        [JsonProperty("iOS")]
        public IOSBuildConfig IOS { get; set; }

        /// <summary>
        /// Parse json string to configuration object. 
        /// </summary>
        /// <param name="configStr">JSON string.</param>
        /// <returns>Configuration data object.</returns>
        public static BuildConfig FromJson(string configStr)
        {
            return JsonConvert.DeserializeObject<BuildConfig>(configStr, Converter.Settings);
        }
    }

    /// <summary>
    /// Class for convrt jSON to object. 
    /// </summary>
    public class Converter
    {
        /// <summary>
        /// Serialize setting for jSON.
        /// </summary>
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}

