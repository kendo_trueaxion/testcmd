﻿namespace TrueAxion.Build
{
    /// <summary>
    /// Enum for every platform.
    /// </summary>
    public enum Platform
    {
        None = 0,
        Android = 1,
        IOS = 2
    }

    /// <summary>
    /// Enum for build arguments
    /// </summary>
    public enum BuildArguments
    {
        ConfigFilePath = 0,
        Platform = 1,
        BuildNumber = 2
    }
}