﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
//using TrueAxion.Log;

namespace TrueAxion.Build
{
    /// <summary>
    /// Main class for Build.
    /// </summary>
    public class BuildService
    {
        private const Platform defaultPlatform = Platform.None;
        private const string defaultConfigPath = "config.json";

        /// <summary>
        /// Function that execute by commandline.
        /// </summary>
        public static void CMDBuild()
        {
            Dictionary<BuildArguments, string> args = GetSpecificArguments(GetCMDArguments());

            Platform platform = (Platform)System.Enum.Parse(typeof(Platform), args[BuildArguments.Platform]);

            try
            {
                int buildNumber = Int32.Parse(args[BuildArguments.BuildNumber]);
                BuildPlatform(platform, args[BuildArguments.ConfigFilePath], buildNumber);
            }
            catch(Exception e)
            {
                //TALogger.Log(LogLevel.Error, LogChannel.BuildService, e);
            }
        }

        /// <summary>
        /// For build a target platform.
        /// </summary>
        /// <param name="platform">Platform that we want to build. Get from GetPlatform function.</param>
        /// <param name="configPath">ConfigPath for set configuration.</param>
        private static void BuildPlatform(Platform platform, string configPath, int buildNumber)
        {
            switch(platform)
            {
                case Platform.Android: BuildAndroid(configPath, buildNumber); break;
                case Platform.IOS: BuildIOS(configPath, buildNumber); break;
            }
        }

        /// <summary>
        /// Get arguments from commandline.
        /// </summary>
        /// <returns>All arguments in commandline.</returns>
        private static string[] GetCMDArguments()
        {
            string[] args = System.Environment.GetCommandLineArgs();
            return args;
        }

        /// <summary>
        /// Data storage that contain form for get argument from commandline.
        /// </summary>
        private static Dictionary<BuildArguments, string> argumentForms = new Dictionary<BuildArguments, string>()
        {
            { BuildArguments.ConfigFilePath, "-config=" },
            { BuildArguments.Platform, "-platform=" },
            { BuildArguments.BuildNumber, "-buildNumber=" }
        };

        /// <summary>
        /// Get specific arguments in commandline arguments.
        /// </summary>
        /// <param name="args">All commandline arguments.</param>
        private static Dictionary<BuildArguments, string> GetSpecificArguments(string[] args)
        {
            Dictionary<BuildArguments, string> specificArguments = new Dictionary<BuildArguments, string>();

            foreach(var form in argumentForms)
            {
                string argumentForm = args.SingleOrDefault(arg => arg.StartsWith(form.Value));
                string argument = argumentForm.Remove(0, form.Value.Length);
                specificArguments.Add(form.Key, argument);
            }

            return specificArguments;
        }

        /// <summary>
        /// Function for build android.
        /// </summary>
        private static void BuildAndroid(string configPath, int buildNumber)
        {
            AndroidBuildOptions androidOptions = new AndroidBuildOptions();

            string configStr = File.ReadAllText(configPath);
            BuildConfig config = BuildConfig.FromJson(configStr);
            androidOptions.SetPlayerSetting(config, buildNumber);
            BuildPlayerOptions options = androidOptions.GetPlatformSpecificOptions(config);
            BuildPipeline.BuildPlayer(options);
        }

        /// <summary>
        /// Function for build iOS.
        /// </summary>
        private static void BuildIOS(string configPath, int buildNumber)
        {
            IOSBuildOptions iOSOptions = new IOSBuildOptions();

            string configStr = File.ReadAllText(configPath);
            BuildConfig config = BuildConfig.FromJson(configStr);
            iOSOptions.SetPlayerSetting(config, buildNumber);
            BuildPlayerOptions options = iOSOptions.GetPlatformSpecificOptions(config);
            BuildPipeline.BuildPlayer(options);
        }
    }
}
