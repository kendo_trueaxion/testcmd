﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour {

    [SerializeField]
    GameObject playerPrefab;
	
	void Start () {

#if CHARACTER_TWIN
        Instantiate(playerPrefab, playerPrefab.transform.position, Quaternion.identity);
#endif

#if CHARACTER_TRIPLE
        Instantiate(playerPrefab, new Vector3(5, 0, 5), Quaternion.identity);
#endif

    }
	

}
